package Tests;

import static org.junit.jupiter.api.Assertions.*;

import static spark.Spark.get;
import static spark.Spark.port;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;

import Constructors.Persistence_Factory;
import Controllers.CDRsController;
import Controllers.PhoneNumbersController;
import Data.CDRRepository;
import Data.ICDRRepository;
import Data.IPhoneNumberRepository;
import Data.PhoneNumberRepository;
import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;
import Main.main;
import Persistence.FileSQL;
import Persistence.FileTxt;
import Persistence.IDataPersistence;
import UseCases.CalculateCostCDR;
import UseCases.ICalculateCostCDR;
import UseCases.ILoadAndSaveCDRandNumbersPersistence;
import UseCases.ILoadCDRsFromExternalFile;
import UseCases.ILoadPhoneNumberFromExternalFile;
import UseCases.IShowCDRs;
import UseCases.IShowPhoneNumbers;
import UseCases.LoadAndSaveCDRandNumbersPersistence;
import UseCases.LoadCDRsFromExternalFile;
import UseCases.LoadPhoneNumbersFromExternalFile;
import UseCases.ShowCDRs;
import UseCases.ShowPhoneNumbers;
import org.junit.After;

import static junit.framework.TestCase.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.stop;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;


class Tests {

	private Phone_Number Telefono_Plan_Postpago = new Phone_Number(1, 1, "PLAN_POSTPAGO","S/N");
	private Phone_Number Telefono_Plan_Prepago = new Phone_Number(1, 1, "PLAN_PREPAGO","S/N");
	private Phone_Number Telefono_Plan_WOW = new Phone_Number(1, 1, "PLAN_WOW","2,3");

	// VERIFY PLANS ARE CREATED CORRECTLY
	@Test
	void should_return_plan_type_postpago() {
		Telefono_Plan_Postpago.showAllPhoneInformation();
		assertEquals("PLAN_POSTPAGO", Telefono_Plan_Postpago.getPlan());
	}
	@Test
	void should_return_plan_type_prepago() {
		Telefono_Plan_Prepago.showAllPhoneInformation();
		assertEquals("PLAN_PREPAGO", Telefono_Plan_Prepago.getPlan());
	}
	@Test
	void should_return_plan_type_wow() {
		Telefono_Plan_WOW.showAllPhoneInformation();
		assertEquals("PLAN_WOW", Telefono_Plan_WOW.getPlan());
	}
	
	// VERIFY COST FOR PREPAGO PLAN

	@Test
	void should_return_cost_7_25() {
		assertEquals(7.25, Telefono_Plan_Prepago.calculateCost("07:00:00", "00:05:00", 1));
	}
	
	@Test
	void should_return_cost_7_98() {
		assertEquals(7.98, Telefono_Plan_Prepago.calculateCost("20:59:59", "00:05:30", 1));
	}
	
	@Test
	void should_return_cost_4_75() {
		assertEquals(4.75, Telefono_Plan_Prepago.calculateCost("21:00:00", "00:05:00", 1));
	}
	
	@Test
	void should_return_cost_5_23() {
		assertEquals(5.23, Telefono_Plan_Prepago.calculateCost("00:59:59", "00:05:30", 1));
	}

	@Test
	void should_return_cost_3_5() {
		assertEquals(3.5, Telefono_Plan_Prepago.calculateCost("01:00:00", "00:05:00", 1));
	}

	
	@Test
	void should_return_cost_3_85() {
		assertEquals(3.85, Telefono_Plan_Prepago.calculateCost("06:59:59", "00:05:30", 1));
	}

	// VERIFY COST FOR POSTPAGO PLAN
	
	@Test
	void should_return_cost_5_5() {
		assertEquals(5.5, Telefono_Plan_Postpago.calculateCost("06:59:59", "00:05:30", 1));
	}
	
	@Test
	void should_return_cost_12_0(){
		assertEquals(12.0, Telefono_Plan_Postpago.calculateCost("06:59:59", "00:12:00", 1));
	}
	
	
	// VERIFY COST FOR WOW PLAN

	@Test
	void should_return_cost_4_95() {
		assertEquals(4.95, Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 1));
	}
		
	//VERIFY FRIENDS NUMBERS
	@Test
	void should_return_0_when_targetnumber_is_123456() {
		Telefono_Plan_WOW.registerFriendNumber(123456);
		assertEquals(0.0 , Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 123456));
	}
	
	@Test
	void should_return_0_when_targetnumber_is_not_123456() {
		Telefono_Plan_WOW.registerFriendNumber(123456);
		assertEquals(4.95, Telefono_Plan_WOW.calculateCost("20:59:59", "00:05:00", 654321));
	}

	
	//FOR CDRs
	@Test
	void Test_methods_get_for_CDRs() {
		CDR register = new CDR(321,1,"00:02:45", "10/11/19", "00:02:45");

		assertEquals(321, register.getOriginNumber());
		assertEquals(1, register.getDestinationNumber());

	}


	//TESTING PHONE_NUMBER ENTITY & REPOSITORY
    @Test
    public void Phone_Number_Registration() {
    	Phone_Number phoneEntity = new Phone_Number(1, 10, "PLAN_PREPAGO", "S/N");
    	String phonenumbers="1;10;PLAN_PREPAGO;S/N";
    	PhoneNumberRepository phoneRepository = new PhoneNumberRepository();
    	phoneRepository.RegisterNewPhoneNumbers(phonenumbers);
    	assertEquals(phoneEntity.getBalance(), phoneRepository.getNewPhoneNumbers().get(0).getBalance());
    	assertEquals(phoneEntity.getAntique(), phoneRepository.getNewPhoneNumbers().get(0).getAntique());
    	assertEquals(phoneEntity.getTelephoneFriends(), phoneRepository.getNewPhoneNumbers().get(0).getTelephoneFriends());
    }

    
    @Test
    public void Phone_Number_Comparation() {
    	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
    	Phone_Number phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
    	ListPhoneNumbers.add(phoneEntity);
    	PhoneNumberRepository phoneRepository = new PhoneNumberRepository();
    	phoneRepository.CleanNewPhoneNumbers();
    	phoneRepository.RegisterNewNumber(phoneEntity);
    	phoneRepository.ShowPhoneNumber();
    	phoneRepository.RegisterFriendNumber(1, 4);
    	assertEquals(ListPhoneNumbers, phoneRepository.getNumbers());
    	assertEquals(phoneEntity, phoneRepository.getNumber(1));
    }
    
    //TESTING CDR ENTITY & REPOSITORY
    @Test
	void CDR_Information() {
		CDR cdrEntity = new CDR(321,1,"00:01:27", "10/11/19", "00:02:45");
		assertEquals("NO TARFICADO", cdrEntity.getdateTarification());
		assertEquals("NO TARFICADO", cdrEntity.gethourTarification());
		assertEquals("00:02:45", cdrEntity.getHour());
		assertEquals("10/11/19", cdrEntity.getDate());
		assertEquals("00:01:27", cdrEntity.getDurationCall());
	}
    
    @Test
	void CDR_Comparation() { 
    	CDRRepository cdrRepository = new CDRRepository();
    	CDR cdrEntity = new CDR(1,2,"00:01:27", "10/11/19", "00:02:45");
    	cdrRepository.CleanListCDRsNotCalculated();
    	cdrRepository.RegisterCDRs("1;2;00:01:27;10/11/19;00:02:45");
    	cdrRepository.ShowAllCDRs();
    	assertEquals(cdrEntity.getDate(), cdrRepository.getCDRsNotCalculated().get(0).getDate());
	}
    
    @Test
	void CDR_Calculate_Rate_For_One() { 
    	CDRRepository cdrRepository = new CDRRepository();
    	Phone_Number phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
    	cdrRepository.CalculateRateForOne(phoneEntity);
    	
	}
    
    @Test
	void Search_Phone_Number() { 
    	CDRRepository cdrRepository = new CDRRepository();
    	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
    	Phone_Number phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
    	ListPhoneNumbers.add(phoneEntity);
    	cdrRepository.SearchPhone(1, ListPhoneNumbers);
    	
	}
    
    @Test
	void Load_CDRs() { 
    	CDRRepository cdrRepository = new CDRRepository();
    	CDR cdrEntity = new CDR(321,1,"00:01:27", "10/11/19", "00:02:45");
    	List<CDR> ListCDRs = new ArrayList<CDR>();
    	ListCDRs.add(cdrEntity);
    	cdrRepository.LoadCDRs(ListCDRs);
	}
    
    //TESTING PERSISTENCE_FACTORY
    @Test
    void Testint_Persistence_Type() { 
    	Persistence_Factory persistence = new Persistence_Factory();
    	IDataPersistence persistenceType = null;
    	persistenceType = new FileSQL();
    	assertEquals(persistenceType.getClass(), persistence.ChangePersistence("SQL_PERSISTENCE").getClass());
    	assertEquals(new FileTxt().getClass(), persistence.ChangePersistence("SERIALIZATED_PERSISTENCE").getClass());
	}
    
    //TESTING SQL_PERSISTENCE
    @Test
    void SQL_Persistence_Type() { 
    	FileSQL sql = new FileSQL();
    	assertEquals("SQL_PERSISTENCE", sql.GetTypePersistence());
	}
    
    @Test
    void SQL_Load_CDR_And_Phone_Numbers() { 
    	FileSQL sql = new FileSQL();
    	sql.LoadDataCDRs();
    	sql.LoadDataPhoneNumbers();
	}
    
    @Test
    void SQL_Save_Phone_Numbers() { 
    	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
    	Phone_Number phoneEntity = new Phone_Number(1, 10, "PLAN_WOW", "2,3");
    	ListPhoneNumbers.add(phoneEntity);
    	FileSQL sql = new FileSQL();
    	sql.SaveDataPhoneNumbers(ListPhoneNumbers);
	}
    
    @Test
    void SQL_Save_CDRs() { 
    	CDR cdrEntity = new CDR(1,2,"00:01:27", "10/11/19", "00:02:45");
    	CDRRepository cdrRepository = new CDRRepository();
    	cdrRepository.RegisterCDR(cdrEntity);
    	FileSQL sql = new FileSQL();
    	sql.SaveDataCDRs(cdrRepository.getCDRsNotCalculated());
	}
    
    //TESTING TXT_PERSISTENCE
    @Test
    void Txt_Persistence_Type() { 
    	FileTxt txt = new FileTxt();
    	assertEquals("SERIALIZATED_PERSISTENCE", txt.GetTypePersistence());
	}
    
    @Test
    void Txt_Load_And_Save_Phone_Numbers() { 
    	List<Phone_Number> ListPhoneNumbers  = new ArrayList<Phone_Number>();
    	FileTxt txt = new FileTxt();
    	ListPhoneNumbers = txt.LoadDataPhoneNumbers();
    	txt.SaveDataPhoneNumbers(ListPhoneNumbers);
	}

    //TESTING CDR CONTROLLER
    @Test
    void Read_File_From_CSV() throws IOException { 
    	ICDRRepository CDRsRepository = new CDRRepository();
    	ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
    	InputStream is = new FileInputStream("D:\\trabajo\\Septimo Semestre\\Arquitectura de software\\ProyectoArquitectura\\CDRs Para Cargar.csv");
    	BufferedReader buf = new BufferedReader(new InputStreamReader(is)); 
    	String line = buf.readLine(); 
    	StringBuilder sb = new StringBuilder(); 
    	while(line != null){ 
    		sb.append(line).append("\n"); 
    		line = buf.readLine(); 
    	} 
    	String fileAsString = sb.toString(); 
    	System.out.println("Contents : " + fileAsString);
    	loadCDRsFromExternalFile.LoadCDRsFromFileCSV(fileAsString);
    	loadCDRsFromExternalFile.CleanCDRsLoaded();
	}
    
    @Test
    void Save_CDR() throws IOException { 
    	IPhoneNumberRepository PhonesRepository = new PhoneNumberRepository();
		ICDRRepository CDRsRepository = new CDRRepository();
		IDataPersistence DataPersistence = new FileSQL();
		IShowPhoneNumbers showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
		ICalculateCostCDR calculateCostCDR = new CalculateCostCDR(CDRsRepository, PhonesRepository);
		IShowCDRs showCDRs = new ShowCDRs(CDRsRepository);
    	calculateCostCDR.CalculateCostForNewCDRs();
    	loadAndSaveCDRandNumbersPersistence.SaveCDRPersistence(showCDRs.ShowCDRsNotCalculated());
    	loadCDRsFromExternalFile.CleanCDRsLoaded();
    	loadAndSaveCDRandNumbersPersistence.GetTypePersistence();
    	loadAndSaveCDRandNumbersPersistence.ChoosePersistence("SQL_PERSISTENCE");
    	
	}
    

	
    //TESTING PHONE NUMBER CONTROLLER
	@Test
    void Teste_get_bill_rate() throws IOException { 
		Bill bill = new Bill(12, "10/11/19", 23);
		bill.setcost(2);
		bill.setmonth("2");
		bill.setphoneNumber(1233);
		bill.getphoneNumber();
		bill.getmonth();
		CDR register1 = new CDR(1, 2, "00:10:0", "10/11/19", "00:02:45");
		CDR register2 = new CDR(1, 2, "00:10:0", "10/11/19", "00:02:45");
		CDR register3 = new CDR(1, 2, "00:10:0", "10/11/19", "00:02:45");
		CDR register4 = new CDR(1, 2, "00:10:0", "10/11/19", "00:02:45");
		register1.setCallCost(10);
		register2.setCallCost(10);
		register3.setCallCost(10);
		register4.setCallCost(10);
		List<CDR> cdrs = new ArrayList<CDR>();
		cdrs.add(register1);
		cdrs.add(register2);
		cdrs.add(register3);
		cdrs.add(register4);
		ICDRRepository repository = new CDRRepository();
		repository.LoadCDRs(cdrs);
		repository.setTarificateDate("01/10/1998");

		bill = repository.getRateForClient(1, "10");
		assertEquals(40.0, bill.getcost() );
	}
	
    

    //TESTING PHONE NUMBER CONTROLLER

    @Test
    void Read_File_From_CSV_Phone_Number_and_methods() throws IOException { 
		IPhoneNumberRepository PhonesRepository = new PhoneNumberRepository();
		ICDRRepository CDRsRepository = new CDRRepository();
		IDataPersistence DataPersistence = new FileSQL();
		IShowPhoneNumbers showPhoneNumbers = new ShowPhoneNumbers(PhonesRepository);
		ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence = new LoadAndSaveCDRandNumbersPersistence(DataPersistence,CDRsRepository,PhonesRepository);
		ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile = new LoadPhoneNumbersFromExternalFile(PhonesRepository);
		ICalculateCostCDR calculateCostCDR = new CalculateCostCDR(CDRsRepository, PhonesRepository);
		ILoadCDRsFromExternalFile loadCDRsFromExternalFile = new LoadCDRsFromExternalFile(CDRsRepository);
		IShowCDRs showCDRs = new ShowCDRs(CDRsRepository);
		InputStream is = new FileInputStream("D:\\trabajo\\Septimo Semestre\\Arquitectura de software\\ProyectoArquitectura\\Numeros Telefonicos para Cargar.csv");
    	BufferedReader buf = new BufferedReader(new InputStreamReader(is)); 
    	String line = buf.readLine(); 
    	StringBuilder sb = new StringBuilder(); 
    	while(line != null){ 
    		sb.append(line).append("\n"); 
    		line = buf.readLine(); 
    	} 
    	showCDRs.ShowCDRsFromPersistence();
    	showCDRs.ShowFilterCDRs();
    	showCDRs.ShowCDRsFilterByDateAndHour("10/11/19", "00:02:45");
    	showPhoneNumbers.ShowPhoneNumbersFromPersistence();
    	String fileAsString = sb.toString(); 
    	loadPhoneNumberFromExternalFile.LoadPhoneNumbersFromFileCSV(fileAsString);
    	loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(showPhoneNumbers.ShowNewPhoneNumbers());
    	loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
		CDRsController controllerCDR= new CDRsController(showCDRs, calculateCostCDR, loadCDRsFromExternalFile, loadAndSaveCDRandNumbersPersistence);
		PhoneNumbersController phoneNumbers= new PhoneNumbersController(loadAndSaveCDRandNumbersPersistence,showPhoneNumbers,loadPhoneNumberFromExternalFile);
		//TESTING METHODS

		controllerCDR.GetInitialPage();
		controllerCDR.GetCost();
		controllerCDR.DeleteCDRsLoaded();
		controllerCDR.RecoverCDRs();
		controllerCDR.SaveCDRS();
		controllerCDR.ChangePersistence();
		controllerCDR.LoadFile();
		controllerCDR.SqlPersitence();
		controllerCDR.FilePersistence();
		controllerCDR.GetFileView();
		controllerCDR.SearchCDR();
		controllerCDR.GetViewCDRsSearched("10/10/1998","20:01:29");
		
		phoneNumbers.SavePhoneNumbers();
		phoneNumbers.DeletePhoneNumbers();
		phoneNumbers.getNewPhoneNumbers();
		phoneNumbers.viewPhoneNumbersloaded();
		phoneNumbers.getViewLoadFile();
		phoneNumbers.getPhoneNumbers();
    }
	@Test
	void TestMain() {
		main.main(null);
	}
}
