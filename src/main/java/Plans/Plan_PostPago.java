package Plans;

import java.io.Serializable;

public class Plan_PostPago implements IPlan,Serializable{
	static final long serialVersionUID = -2555170365942325277L;
	private static final double UNIQUE_PRICE = 1;
	@Override
	public double calculateCost(String hour, String duration, int targetTelephone) {
		double cost;
		cost = betweenTime.convertHourToSeconds(duration) * (UNIQUE_PRICE/60);
		cost = Math.round(cost * 100.0) / 100.0;
		return cost;
	}
	@Override
	public String getPlanType() {
		return "PLAN_POSTPAGO";
	}
}
