package UseCases;

import java.util.List;

import Data.ICDRRepository;
import Entities.CDR;

public class ShowCDRs implements IShowCDRs{

	ICDRRepository CDRRepository;
	public ShowCDRs(ICDRRepository CDRRepository) {
		this.CDRRepository = CDRRepository;
	}
	@Override
	public List<CDR> ShowCDRsNotCalculated() {
		return CDRRepository.getCDRsNotCalculated();
	}

	@Override
	public List<CDR> ShowCDRsFromPersistence() {
		return CDRRepository.getCDRsCalculated();
	}
	@Override
	public List<CDR> ShowFilterCDRs() {
		return CDRRepository.getCalculatedDateFromCDRs();
	}
	@Override
	public List<CDR> ShowCDRsFilterByDateAndHour(String Date, String Hour) {
		return CDRRepository.getCDRFilterByDateAndHour(Date, Hour);
	}

}
