package Data;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import Entities.Bill;
import Entities.CDR;
import Entities.Phone_Number;

public class CDRRepository implements ICDRRepository{

	List<CDR> ListCDRsCalculated = new ArrayList<CDR>(); 
	List<CDR> ListCDRsNotCalculated = new ArrayList<CDR>(); 
	private List<CDR> ListResult ;
	
	
	public List<CDR> getCalculatedDateFromCDRs() {
        ListResult = new ArrayList<CDR>();
        String CurrentDate = null;
        String CurrentHour = null;
        for (CDR cdr : ListCDRsCalculated) {
        	if (!cdr.gethourTarification().equals(CurrentHour) || !cdr.getdateTarification().equals(CurrentDate))
        	{
        		CurrentDate = cdr.getdateTarification();
        		CurrentHour = cdr.gethourTarification();
        		ListResult.add(cdr);
        	}
    	}
        return ListResult;
    }
	

	public List<CDR> getCDRFilterByDateAndHour(String CurrentDate, String CurrentHour) {
        ListResult = new ArrayList<CDR>();
        for (CDR cdr : ListCDRsCalculated) {
        	if (cdr.gethourTarification().equals(CurrentHour) && cdr.getdateTarification().equals(CurrentDate))
        	{
        		ListResult.add(cdr);
        	}
    	}
        return ListResult;
    }
	
	@Override
	public void LoadCDRs(List<CDR> ListCDRs) {
		ListCDRsCalculated.clear();
		ListCDRsCalculated.addAll(ListCDRs);
	}
	
	@Override
	public List<CDR> getCDRsCalculated() {
		return ListCDRsCalculated;
	}

	@Override
	public List<CDR> getCDRsNotCalculated() {
		return ListCDRsNotCalculated;
	}
	
	@Override
	public void RegisterCDR(CDR cdr) {
		ListCDRsNotCalculated.add(cdr);
	}

	@Override
	public void ShowAllCDRs() {
		for (CDR cdr : ListCDRsCalculated) {
			cdr.showInformation();
		}
		for (CDR cdr : ListCDRsNotCalculated) {
			cdr.showInformation();
		}
	}

	@Override
	public List<CDR> CalculateRateForOne(Phone_Number number) {
		List<CDR> result = SearchCDRs(number);
		for (CDR cdr : result) {
			cdr.setCallCost(cdr.calculteCallCost(number));
		}
		return result;
	}

	@Override
	public List<CDR> CalculateRateForAll(List<Phone_Number> listPhoneNumber) {
		String CurrentHour = currentDate.getHour();
		String CurrentDate = currentDate.getDate();
		for(CDR register : ListCDRsNotCalculated)
		{
			Phone_Number phone = SearchPhone(register.getOriginNumber(),listPhoneNumber);
			register.setCallCost(register.calculteCallCost(phone));
			register.SetdateTarification(CurrentDate);
			register.SethourTarification(CurrentHour);
		}
		return ListCDRsNotCalculated;
	}

	public Phone_Number SearchPhone(int numberOrigin,List<Phone_Number> listPhoneNumber) {
		for(Phone_Number phone : listPhoneNumber)
		{
			if(phone.getTelephone() == numberOrigin)
				return phone;
		}
		return null;
	}
	
	
	private List<CDR> SearchCDRs(Phone_Number number){
        ListResult = new ArrayList<CDR>();
		for (CDR cdr : ListCDRsNotCalculated) {
			if (cdr.getOriginNumber() ==  number.getTelephone()) {
				ListResult.add(cdr);
			}
		}
		return ListResult;
	}

	
	
	@Override
	public void RegisterCDRs(String CDRs) {
		int originNumber = 0, destinationNumber = 0;
		String durationCall = null, date = null, hour = null;
		String[] parts = CDRs.split("\\r?\\n|;");
		for(int i=0;i<parts.length;i++) {
			originNumber=Integer.parseInt(parts[i]);i++;
			destinationNumber=Integer.parseInt(parts[i]);i++;
			durationCall=parts[i];i++;
			date=parts[i];i++;
			hour=parts[i];	
			CDR newCDR = new CDR(originNumber, destinationNumber, durationCall, date, hour);
			ListCDRsNotCalculated.add(newCDR);
		}
	}

	@Override
	public void CleanListCDRsNotCalculated() {
		ListCDRsNotCalculated.clear();
	}

	@Override
	public Bill getRateForClient(int phoneNumber, String date) {
		Bill NewBill = new Bill(phoneNumber, date, 0);
		for (CDR cdr : ListCDRsCalculated) {
			if (ValidateNumberAndDate(phoneNumber,date,cdr))
			{
				NewBill.setcost(NewBill.getcost()+cdr.getCallCost());
			}
		}
		return NewBill;
	}
	
	private boolean ValidateNumberAndDate(int phoneNumber, String date,CDR cdr) {
		if (phoneNumber == cdr.getOriginNumber()  && date.contains(GetMonthFromDate(cdr.getdateTarification())))
			return true;
		return false;
	}
	
	private String GetMonthFromDate(String date) {
		String str[] = date.split("/");
		int month = Integer.parseInt(str[1]);
		String result = String.valueOf(month);
		return result;
	}


	@Override
	public void setTarificateDate(String date) {
		for (CDR cdr : ListCDRsCalculated) {
			cdr.SetdateTarification(date);
		}
	}
}
