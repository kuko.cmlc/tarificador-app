package Controllers;
import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import UseCases.ICalculateCostCDR;
import UseCases.ILoadAndSaveCDRandNumbersPersistence;
import UseCases.ILoadPhoneNumberFromExternalFile;
import UseCases.IShowPhoneNumbers;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;

public class PhoneNumbersController {
	ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence;
	IShowPhoneNumbers showPhoneNumbers;
	ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile;
	public PhoneNumbersController(ILoadAndSaveCDRandNumbersPersistence loadAndSaveCDRandNumbersPersistence,
									IShowPhoneNumbers showPhoneNumbers,
									ILoadPhoneNumberFromExternalFile loadPhoneNumberFromExternalFile
									) {
		this.loadAndSaveCDRandNumbersPersistence = loadAndSaveCDRandNumbersPersistence;
		this.showPhoneNumbers = showPhoneNumbers;
		this.loadPhoneNumberFromExternalFile = loadPhoneNumberFromExternalFile;
		loadAndSaveCDRandNumbersPersistence.LoadPhoneNumbersPersistence();
	}
	
	public void Methods() {
		//http://localhost:8080/getNumbers //For Open de IU
        get("/getNumbers", (request, response) ->
        {
        	return getPhoneNumbers();
        });
        
        get("/loadFileNumbers", (request, response) -> {
        	return getViewLoadFile();
        });
                
        post("/api/submitPhoneNumbers", (req,res)->{
        	req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
        	Part uploadedFile=null;
        	try {
        		uploadedFile=req.raw().getPart("myFile");
        	}
        	catch (IOException | ServletException e) {
        		e.printStackTrace();
        	}
        	try(InputStream inStream = uploadedFile.getInputStream()){
        		StringWriter writer = new StringWriter();
        		IOUtils.copy(inStream, writer);
        		String theString = writer.toString();
        		System.out.println("Contetn from uploaded file: " + theString);
        		loadPhoneNumberFromExternalFile.LoadPhoneNumbersFromFileCSV(theString);
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	return viewPhoneNumbersloaded();
        });

        
        get("/getNewPhoneNumbers", (request, response) ->
        {
        	return getNewPhoneNumbers();
        });        

        get("/DeletePhoneNumbersLoaded", (request, response) ->
        {
        	return DeletePhoneNumbers();
        });
     
        get("/SaveNewPhoneNumbers", (request, response) ->
        {
        	return SavePhoneNumbers();
        });
	}
	
	public Object getPhoneNumbers() {
    	Map<String, Object> model = new HashMap<>();
		loadAndSaveCDRandNumbersPersistence.LoadPhoneNumbersPersistence();
    	model.put("Numbers",showPhoneNumbers.ShowPhoneNumbersFromPersistence());
    	model.put("TotalPhoneNumbers",showPhoneNumbers.ShowPhoneNumbersFromPersistence().size());        	
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
		return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/phoneNumbers/numbers.vm"));
	}
	
	public Object getViewLoadFile() {
    	Map<String, Object> model = new HashMap<>();
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/loadFile/IULoadPhoneNumbers.vm"));
	}
	
	public Object viewPhoneNumbersloaded() {
     	Map<String, Object> model = new HashMap<>();
    	model.put("Numbers", showPhoneNumbers.ShowNewPhoneNumbers());
    	model.put("TotalPhoneNumbers",showPhoneNumbers.ShowNewPhoneNumbers().size());
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/phoneNumbers/PreRegisterPhoneNumbers.vm"));
	}
	
	public Object getNewPhoneNumbers() {
       	Map<String, Object> model = new HashMap<>();
    	model.put("Numbers", showPhoneNumbers.ShowNewPhoneNumbers());
    	model.put("TotalPhoneNumbers",showPhoneNumbers.ShowNewPhoneNumbers().size());
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/phoneNumbers/PreRegisterPhoneNumbers.vm"));
	}
	
	public Object DeletePhoneNumbers() {
		Map<String, Object> model = new HashMap<>();
    	loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
    	model.put("Numbers", showPhoneNumbers.ShowNewPhoneNumbers());
    	model.put("TotalPhoneNumbers",showPhoneNumbers.ShowNewPhoneNumbers().size());
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/phoneNumbers/PreRegisterPhoneNumbers.vm"));
	}
	
	public Object SavePhoneNumbers() {
		Map<String, Object> model = new HashMap<>();
    	model.put("Message","�Los Nuevos numeros telefonicos se Guardaron Con Exito!");
    	loadAndSaveCDRandNumbersPersistence.SavePhoneNumbersPersistence(showPhoneNumbers.ShowNewPhoneNumbers());
    	loadPhoneNumberFromExternalFile.CleanPhoneNumbersLoaded();
    	model.put("Persistence",loadAndSaveCDRandNumbersPersistence.GetTypePersistence());
		new VelocityTemplateEngine().render(new ModelAndView (model,"velocity/nav.vm"));
    	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/PersistenceCDRsTable/SaveMessage.vm"));
	}
}
